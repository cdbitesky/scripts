#! /bin/sh
 
if [ $# -gt 0 ]; then
    cd "$1";
fi
	 
ls -1 | awk 'BEGIN {srand()}
	{x[NR] = $0}
	END {print "Selected", x[1 + int(rand() * NR)]}'
