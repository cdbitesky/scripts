#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
This script will iterate through all elements in a directory.
File:
	Create a directory for originization of the same name as the file.
Dir:
	Search for the movie (IMDB as the current DB) and format directory
	 name with appended date or release.
TODO:
	-- AKA for an optional dirname (kung fu hustle skips for some reason)
	-- Collections

'''
import sys, os, shutil
import urllib2, urllib, json
import argparse, gzip, hashlib, re
import threading
import Queue

api_base = "http://mymovieapi.com/"
md_filename = 'metadata.gz'

class HasherThread(object):
	def __init__(self):
		self.worker_thread = None
		self.thread_queue = Queue.Queue()
		self.current_thread = None
	def put(self, another_thread):
		self.thread_queue.put(another_thread)
	def do_work(self):
		while not self.thread_queue.empty():
			if not self.current_thread:
				self.current_thread = self.thread_queue.get()
			self.current_thread.start()
			self.current_thread.join()
			self.current_thread = None
	def start_work(self):
		if not self.worker_thread:
			self.worker_thread = threading.Thread(target=HasherThread.do_work ,args = (self,))
			self.worker_thread.start()
			self.worker_thread.join()
			self.worker_thread = None
	def join(self):
		if self.worker_thread.isAlive():
			self.worker_thread.join()

def main():
	parser = parser_setup()
	args = parser.parse_args()
	if args.verbose:
		print args
	items_to_fetch = []
	item_data = Queue.Queue()
	hasher = HasherThread()
	dirpath = args.directory or os.getcwd()

	if not args.delete_metadata:
		# Collect any direct files into folders
		if not args.ignore_files:
			for item in os.listdir(dirpath):
				if( item[0] != '.' ):
					if os.path.isfile(os.path.join(dirpath,item)):
						if args.verbose:
							print "f:",item
						if ( not os.path.exists(os.path.join(dirpath,item+"_dir")) ):
							# Create the directory, move file in, then rename the dir
							print "Creating a directory for:", item
							os.makedirs(os.path.join(dirpath,item+"_dir"))
							print "Moving",item
							shutil.move(os.path.join(dirpath,item),
										os.path.join(dirpath,item+"_dir"))
							if not os.path.isdir(os.path.join(dirpath,item)):
								os.rename(os.path.join(dirpath,item+"_dir"),
										os.path.join(dirpath,item))
							else:
								print "Directory:",os.path.join(dirpath,item),"already exists"
		for item in os.listdir(dirpath):
			if( item[0] != '.' ):
				if os.path.isdir(os.path.join(dirpath,item)):
					if args.verbose:
						print "d:",item
					if not args.traverse:
							if not md_check(hasher, os.path.join(dirpath,item),args.check_all):
								items_to_fetch.append(item)
		print "Staring movie information retrieval"
		threads = [threading.Thread(target=get_movie_info, args = (item_data,item,args.num_selection, args.verbose)) for item in items_to_fetch]
		for t in threads:
			t.start()
		for t in threads:
			t.join()
		print "Finishing movie information retrieval"

		while not item_data.empty():
			current_item = item_data.get()
			item = current_item[0]
			j = current_item[1]
			(j_pos, aka_flag) = is_correct_movie(item, j)
			if j_pos == -1:
				break
			if j_pos == 0:
				print "You aren't changing: {title}".format( title = item )
			elif j_pos >= 1:
				new_name = encode_dir(j[j_pos-1],aka_flag)
				rename_dir(dirpath, item, new_name )
				print new_name, "has finished"
				thread = threading.Thread(target=md_generate, args = ( j[j_pos-1],os.path.join(dirpath,new_name)))
				hasher.put(thread)
				threading.Thread(target=hasher.start_work, args =()).start()
		print "Waiting for threads to finish"
		hasher.join()
		print "Finished"

	else:
		print "Removing all metadata"
		if not os.path.exists(dirpath):
			raise Exception('Path given does not exist',dirpath)

		try:
			for movie in os.listdir(dirpath):
				for root, dirs, files in os.walk(os.path.join(dirpath,movie)):
					for names in files:
						if names == md_filename:
							print "Removing:", os.path.join(root,names)
							os.remove(os.path.join(root,names))

		except:
			import traceback
			traceback.print_exc()
			raise
		print "Finished removing all metadata"

def parser_setup():
	parser = argparse.ArgumentParser(description='Process and orginize a directory of movies.')
	parser.add_argument("-v", "--verbose",\
			help="increase output verbosity", action="store_true")
	parser.add_argument("-n", "--num-selection", type=int,\
			help="number of matches in search")
	parser.add_argument("-if", "--ignore-files",\
			help="ignore files in base dir", action="store_true")
	parser.add_argument("-t", "--traverse",\
			help="traverse without any webcalls", action="store_true")
	parser.add_argument("directory", type=str,\
			help="directory to orginize", nargs='?')
	parser.add_argument("-d", "--delete-metadata", \
			help="clear all metadata", action="store_true")
	parser.add_argument("-c", "--check-all", \
			help="check all metadata", action="store_true")
	return parser

def get_movie_info( queue, basic_title, max_selections, verbose_flag ):
	'''
	Retrieve possible matching movies to that of IMDB
	IN: Basic movie title given by the folder name
	OUT: JSON Format output from imdb api call
	'''
	request_url = api_base+'?title='+decode_dir(basic_title)+"&type=json&plot=simple&episode=1&limit="+str(max_selections)+"&yg=0&mt=none&lang=en-US&offset=&aka=simple&release=simple&business=0&tech=0"
	import socks
	import socket
	socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", 9050)
	socket.socket = socks.socksocket
	#request_url = urllib.quote(request_url)
	if verbose_flag:
		print "url",request_url
	# Possibly extract the extra restrains to command arguments
	try:
		response = urllib2.urlopen(request_url)
	except urllib2.URLError as e:
		sys.stderr.write("URLError: {error}".format(error=e.reason))
		raise
	j = json.loads(response.read())
	if verbose_flag:
		print basic_title, "has", len(j), "matching titles"
	if not isinstance(j, list) and j['code'] == 404:
		print "Movie of title doesn't exists in DB: " + basic_title
		print "\t?title="+decode_dir(basic_title)
		return
		#raise MovieNotFoundException(basic_title)
	if verbose_flag:
		for x in j:
			print "title:", x['title']
	queue.put((basic_title, j))

def is_correct_movie( current_name, retrieved_information ):
	'''
	If there is any ambiguity in the data retrieval ask the user for
	 calification.
	IN: JSON Info
	Out: Numerical Selection of the correct movie choice
	'''
	if retrieved_information == "":
		raise Exception('retrieved information', 'was empty')

	print "Current: {cn}".format( cn=current_name )
	for x in range(len(retrieved_information)):
		try:
			if 'year' in retrieved_information[x].keys():
				print u'{count}:"{title} ({year})"'.format( count=(x+1), title=retrieved_information[x]['title'], year=str(retrieved_information[x]['year']) )
				if "also_known_as" in retrieved_information[x].keys():
					for y in range(len(retrieved_information[x]['also_known_as'])):
						print u"\t\"{count}:{title} ({year})\"".format( count = (y+1), title = retrieved_information[x]["also_known_as"], year=str(retrieved_information[x]['year']))
		except KeyError:
			print (x+1)
			print retrieved_information
			raise
	var = raw_input("Please select the closest matching [0 to not change]")
	while not var and not var.isdigit() and int(var) >= -1 and int(var) < len(retrieved_information):
		var = raw_input("Please choose an acceptible number. Please select the closest matching [0 to not change]")
	aka = 0
	if not var:
		print "EMPTY"
		return (0,aka)
	if int(var) <= 0:
		return (int(var),aka)
	if "also_known_as" in retrieved_information[x].keys():
		aka = raw_input("Would you prefer the main title or the alternate[0/1]")
		while not aka and not aka.isdigit() and int(var) >= -1 and int(var) < len(retrieved_information['also_known_as']):
			aka = raw_input("Please choose either 0 or 1")
	return (int(var),int(aka))

def rename_dir( dir_base, old_name, new_name ):
	'''
	Basic rename using os.rename after checking the new name isn't taken
	Handles dir already exists
	'''
	new_name = new_name.replace(u'·',u' ')
	new_name = new_name.replace(u'î',u'i')
	old_name = old_name.replace(u'·',u' ')
	old_name = old_name.replace(u'î',u'i')
	old = os.path.join(dir_base,old_name+u'/')
	new = os.path.join(dir_base,new_name+u'/')
	if old == new:
		return True
	try:
		print u"Replacing '{old}' with '{new}'".format( \
			old = old_name, new=new_name )
	except UnicodeDecodeError:
		print old
		print new
		raise
	if not os.path.exists(new):
		os.rename(old,new)
		return True
	else:
		print u"Path '{name}' already exists".format( name=new_name )
		return False

def encode_dir( movie_information, aka_flag ):
	'''
	Append Year in format '(YYYY)'
	'''
	print "aka_flag:",aka_flag
	if aka_flag>0:
		print "alt list:",movie_information["also_known_as"]
		directory_name = movie_information["also_known_as"][aka_flag-1]
	else:
		directory_name = movie_information['title']
	directory_name = re.sub(r'\s*,\s*',r',',directory_name)
	directory_name = re.sub(r'\s+',r' ',directory_name)
	directory_name = directory_name + '(' + str(movie_information['year']) + ')'
	return directory_name

def decode_dir( dir_name ):
	ret_dir = dir_name
	ret_dir = re.sub('[^\(]\d{4}[^\)].*$','',ret_dir)
	ret_dir = re.sub('TS.*$','',ret_dir,re.IGNORECASE)
	ret_dir = re.sub('uncut.*$','',ret_dir,re.IGNORECASE)
	ret_dir = re.sub('unrat.*$','',ret_dir,re.IGNORECASE)
	ret_dir = re.sub('dvdrip.*$','',ret_dir,re.IGNORECASE)
	ret_dir = re.sub('directors.*cut.*$','',ret_dir,re.IGNORECASE)
	ret_dir = re.sub('xvid.*$','',ret_dir,re.IGNORECASE)
	ret_dir = re.sub('\s*\+\s*','\+',ret_dir)
	ret_dir = re.sub('\s*\(.*\).*','',ret_dir)
	ret_dir = re.sub('\s*\[.*\].*','',ret_dir)
	ret_dir = ret_dir.replace(' ',r'+')
	ret_dir = ret_dir.replace('.',r'+')
	ret_dir = ret_dir.replace(',',r'+')
	ret_dir = ret_dir.replace('_',r'+')
	ret_dir = ret_dir.replace('-',r'+')
	ret_dir = ret_dir.replace('!','')
	ret_dir = ret_dir.replace('\'','')
	return ret_dir

def md_check( hasher, directory, check ):
	'''
	Metadata check to prevent any unessecary webcalls
	'''
	# check if md file exists
	#print "dir:",directory
	if not os.path.exists(os.path.join(directory,md_filename)):
		print "Metadata file doesn't exists for",directory
		return False
	if check:
		thread = threading.Thread(target=hash_check, args = (directory,))
		hasher.put(thread)
		threading.Thread(target=hasher.start_work, args =()).start()
	return True

def hash_check( directory ):
	'''
	Threaded checking
	'''
	#TODO log file for failed hash comparisons
	# retrieve md data
	file = gzip.GzipFile(os.path.join(directory,md_filename),'rb')
	buffer = ''
	while 1:
		data = file.read()
		if data == "":
			break
		buffer += data
	md = json.loads(buffer)
	file.close()
	#compare hashes
	md5 = get_dir_hash(directory)
	if md5 == md['hash']:
		print "\tHash matches for", directory
	else:
		print "\tHash doesn't match for", directory

def md_generate( movie_information, directory ):
	'''
	MD Format(Compressed Json):
		Movie Title:	Char*	(255 Max)
		IMDB ID:		Char*	(255 Max)
		IMDB URL:		Char*	(255 Max)
		Directors:		Char**	(255 Max of 255 Max)
		Writers:		Char**	(255 Max of 255 Max)
		Actors:			Char**	(255 Max of 255 Max)
		Genres:			Char**	(255 Max of 255 Max)
		Plot_Simple:	Char**	(255 Max of 255 Max)
		Length(Minutes):Int
		Release Year:	Date	(Prefered format YYYY)
		MD5 Hash of the Directory (Excluding this file)
	'''
	if not os.path.exists(directory):
		print "Directory doesn't exists:", directory
		return False
	md = {}
	keys = movie_information.keys()
	for x in ['title','id','url','directors','writers','actors','genres','plot','runtime','year']:
		if x in keys:
			md[x] = movie_information[x]
		else:
			md[x] = ""
	md['hash'] = get_dir_hash(directory)
	print md['hash'], directory
	file = gzip.GzipFile(os.path.join(directory,md_filename), 'wb')
	file.write(json.dumps(md))
	file.close()
	return md

def md_collection_generate( movie_information, directory ):
	'''
	MD Format(Compressed Json):
		Count		Int
		MD5Hash		Generated by taking the hash of all md files
	'''

	try:
		for root, dirs, files in os.walk(directory):
			for names in files:
				if names != md_filename:
					continue
				filepath = os.path.join(root,names)
				try:
					f1 = open(filepath, 'rb')
				except:
					# You can't open the file for some reason
					f1.close()
					continue

				while 1:
				# Read file in as little chunks
					buf = f1.read(4096)
					if not buf : break
					hasher.update(hashlib.sha1(buf).hexdigest())
				f1.close()

	except:
		import traceback
		# Print the stack traceback
		traceback.print_exc()
		return -2

def get_dir_hash(directory):
	hasher = hashlib.md5()

	if not os.path.exists(directory):
		##TODO throw
		return -1

	try:
		for root, dirs, files in os.walk(directory):
			for names in files:
				if names == md_filename:
					continue
				filepath = os.path.join(root,names)
				try:
					f1 = open(filepath, 'rb')
				except:
					# You can't open the file for some reason
					f1.close()
					continue

				while 1:
				# Read file in as little chunks
					buf = f1.read(4096)
					if not buf : break
					hasher.update(hashlib.sha1(buf).hexdigest())
				f1.close()

	except:
		import traceback
		# Print the stack traceback
		traceback.print_exc()
		return -2

	return hasher.hexdigest()

if __name__ == "__main__":
	main()
