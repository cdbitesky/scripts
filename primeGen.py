from itertools import combinations, chain

def main():
    p = [ 2, 3 ]
    l = [[]]
    test = 5991157
    #1522605027922533360535618378132637429718068114961380688657908494580122963258952897654000350692006139L
    pos = 1
    old = []
    t = -1
    while t == -1 or p[-1] < test:
    #for x in range(4):
        #p=step(p, p[pos])
        (p, l)=step(p, l, pos)
        p.sort()
        l.sort(key=lambda x: len(x))
        
        #print len(p), p
        #print l

        if( total(p) > test**.5 ):
            print "Possible here"
        print test/1.0/total(p)
        (t, old) = check(p, test, old)
        if( t > -1 ):
            print t
            break
        pos += 1

    #print p
    #print l

def step( primes, l, pos ):
    print "\tEntering Step. New prime:", primes[pos]
    odd_primes = primes[1:pos]
    #print "odd:", odd_primes
    #l = list(powerset(odd_primes))
    l = sPowerset( l, primes[pos] )
    #print "#",l
    for x in l:
        #print "power: ", x
        sum = 0
        for y in x:
            #print y
            sum += y
        if( sum == 0 ):
            sum = 1
        a = sum*2*primes[pos]-1
        b = sum*2*primes[pos]+1
        if( simplePrimeCheck(primes, a) ):
            primes.append(a)
            #print "\t Append:", a
        if( simplePrimeCheck(primes, b) ):
            primes.append(b)
            #print "\t Append:", b
    print "\tExiting Step"
    return primes, l
            
def simplePrimeCheck( p, n ):
    for x in p:
        if( n%x == 0 ):
            #print n,"returns on",x
            return False
    return True

def powerset(seq):
    """ Returns all the subsets of this set. This is a generator. """
    if len(seq) > 1:
        for item in powerset(seq[1:]):
            yield [seq[0]]+item
            yield item
    else:
        yield seq
        yield []

def sPowerset( s, n ):
    ''' s = [ [], [3], [5], [3,5] ] n = 7
        returns [ [], [3], [5], [7], [3,5], [3,7], [5,7], [3,5,7]]
    '''
    #print "s",s
    #print "n",n

    m = []
    for x in s:
        h = x[:]
        h.append(n)
        m.append(h)
    for x in m:
        s.append(x)
    #print s
    #print "Exit"
    return s

def total( list_of_primes ):
    ''' [3,5,7] = 2*3*5*7 210 '''
    ret = 2
    for x in list_of_primes:
        ret *= x
            
    return ret

def mult( subset ):
    mul = 1
    for x in subset:
        mul *= x
    mul *= 2
    return mul

def check( primes, fact, old ):
    ''' given a set of primes check for the factor of fact '''
    pp = [x for x in powerset(primes[1:]) if x not in old] #not including 2
    for x in pp:
        if x: # not []
            tmp = mult(x)
            if ( fact%(tmp-1)==0 ):
                return (tmp-1, [[]])
            if ( fact%(tmp+1)==0 ):
                return (tmp+1, [[]])
    #print "pp:",pp
    #print "old:",old
    #print "old+pp",(old+pp)
    return (-1, old+pp)

if __name__ == "__main__":
    main()
