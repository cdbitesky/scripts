import os, sys, hashlib

if len(sys.argv) == 1:
    print "Not enough arguments"
elif(len(sys.argv)==2):
    pwd = sys.argv[1]
    print pwd
    count = 0
    for f in os.listdir(pwd):
        if not os.path.isdir(os.path.join(pwd,f)) and\
            f != "https:\\\\images.4chan.org\\d\\src\\1344429956822.jpg.desktop":
            fin = open(os.path.join(pwd,f), 'r')
            rep = 'a'+hashlib.md5(fin.read()).hexdigest()+f[f.rfind('.'):]
            fin.close()
            if rep in os.listdir(pwd) and rep != f:
                nn = 'a'+hashlib.md5(f).hexdigest()+f[f.rfind('.'):]
                print rep, nn
                os.rename(os.path.join(pwd,f),os.path.join(pwd,nn))
                count += 1
            else:
                os.rename(os.path.join(pwd,f),os.path.join(pwd,rep))
