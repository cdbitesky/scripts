#!/bin/bash

rsync -auv /home/cdbitesky/.vim/ /home/cdbitesky/projects/dotfiles/vim --exclude tmp --exclude backup --exclude .netrwhist
rsync -auv /home/cdbitesky/.vimrc /home/cdbitesky/projects/dotfiles/vim/vimrc
