#!/bin/bash
BASE=/home/cdbitesky/projects/scripts/rsync/
VIM=${BASE}vim_sync.sh
DOT=${BASE}dot_sync.sh
GIT=${BASE}gitconfig_sync.sh
CONKY=${BASE}conky_sync.sh
FF=${BASE}firefox_sync.sh

if [ -f $DOT ]
then
    echo running "$DOT"
    sh $DOT
    echo "$DOT" finished
else
    echo "$DOT" doesn\'t exist
fi

if [ -f $VIM ]
then
    echo running "$VIM"
    sh $VIM
    echo "$VIM" finished
else
    echo "$VIM" doesn\'t exist
fi

if [ -f $GIT ]
then
    echo running "$"
    sh $GIT
    echo "$GIT" finished
else
    echo "$GIT" doesn\'t exist
fi

if [ -f $CONKY ]
then
    echo running "$"
    sh $CONKY
    echo "$CONKY" finished
else
    echo "$CONKY" doesn\'t exist
fi

if [ -f $FF ]
then
    echo running "$"
    sh $FF
    echo "$FF" finished
else
    echo "$FF" doesn\'t exist
fi

echo
echo completed on `date`
