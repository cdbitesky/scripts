#!/bin/bash

function git_check {
	[ -d .git ] || git rev-parse --git-dir > /dev/null 2>&1
}

function usage {
	echo "usage: $0 options"

	echo "OPTIONS:"
	echo "-h Show this message"
}

function status {
	current=`pwd`
	for D in $@; do
		if $VERBOSE; then
			echo "$current/${D}"
		fi
		if [ -e $current/$D ]; then
			if [ ! -d $current/$D ]; then
				if $VERBOSE; then
					echo "That isn't a directory"
				fi
			else
				pushd $current >/dev/null
				cd "$current/$D"
				if [ git_check ]; then
					if ! $VERBOSE; then
						echo "$current/${D}"
					fi
					git status -vs
				fi
				popd >/dev/null
			fi
		else
			if $VERBOSE; then
				echo "That doesn't exist"
			fi
		fi
	done
}

VERBOSE=false

while getopts "hrsv" OPTION
do
	case $OPTION in
		h)
			usage
			exit 1
			;;
		r)
			;;
		s)
			status $@
			exit 1
			;;
		v)
			VERBOSE=true
			;;
		?)
			usage
			exit 1
			;;
	esac
done
